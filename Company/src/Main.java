import details.Engine;
import entities.Person;
import professions.Driver;
import vehicles.Car;
import vehicles.Lorry;
import vehicles.SportCar;

public class Main
{

    public static void main(String[] args)
    {

        //1
        Engine engine = new Engine(1020, "Франція");
        String powerEngine = engine.toString();
        System.out.println(powerEngine);

        //2
        Person person = new Person("Іваненко Михайло Вікторович", 30, "Чоловік", "+38(067)533-12-34");
        String result = person.toString();
        System.out.println(result);

        //3
        Driver driver = new Driver("Іваненко Михайло Вікторович", 30, "Чоловік", "+38(067)533-12-34", 20);
        String drivingExperience = driver.toString();
        System.out.println(drivingExperience);

        //4
        Car car = new Car("Bugatti", "E-class", 2.5, "GAS", driver, engine);
        String carName = car.toString();
        System.out.println(carName);
        car.start();
        car.stop();
        car.turnRight();
        car.turnLeft();
        System.out.println();

        //5
        Lorry lorry = new Lorry("Man", "N2", 5, "Diesel", driver, engine, 600);
        String Lorry = lorry.toString();
        System.out.println(Lorry);
        lorry.start();
        lorry.stop();
        lorry.turnRight();
        lorry.turnLeft();
        System.out.println();


        //6
        SportCar sportCar = new SportCar("Bugatti", "hypercar", 2.5, "Gas", driver, engine, 407);
        String carBrandBugatti = sportCar.toString();
        System.out.println(carBrandBugatti);
        sportCar.start();
        sportCar.stop();
        sportCar.turnRight();
        sportCar.turnLeft();
        System.out.println();
    }
}