package vehicles;

import details.Engine;
import professions.Driver;

public class Car
{
     private String carBrand;
     private String carClass;
     private double weight;
     private String engineType;

     private Driver driver;
     private Engine engine;
     //

    public Car(String carBrand, String carClass, double weight, String engineType, Driver driver, Engine engine)
    {
        this.carBrand = carBrand;
        this.carClass = carClass;
        this.weight = weight;
        this.engineType = engineType;
        this.driver = driver;
        this.engine = engine;
    }

    //
    public void setCarBrand(String carBrand)
    {
        this.carBrand = carBrand;
    }

    public void setCarClass(String carClass)
    {
        this.carClass = carClass;
    }

    public void setWeight(double weight)
    {
        this.weight = weight;
    }

    public void setEngineType(String engineType)
    {
        this.engineType = engineType;
    }

    public String getCarBrand()
    {
        return carBrand;
    }

    public String getCarClass()
    {
        return carClass;
    }

    public double getWeight()
    {
        return weight;
    }

    public String getEngineType()
    {
        return engineType;
    }

    public Driver getDriver()
    {
        return driver;
    }

    public void setDriver(Driver driver)
    {
        this.driver = driver;
    }

    public Engine getEngine()
    {
        return engine;
    }

    public void setEngine(Engine engine) { this.engine = engine; }

    //
    public void start()
    {
        System.out.println("Поїхали!");
    }

    public void stop()
    {
        System.out.println("Зупиняємося!");
    }

    public void turnRight()
    {
        System.out.println("Поворот направо!");
    }

    public void turnLeft()
    {
        System.out.println("Поворот наліво!");
    }

    //
    @Override
    public String toString() {
        return "Car{" +
                "carBrand='" + carBrand + '\'' +
                ", carClass='" + carClass + '\'' +
                ", weight=" + weight +
                ", engineType='" + engineType + '\'' +
                ", driver=" + driver +
                ", engine=" + engine +
                '}';
    }
}
