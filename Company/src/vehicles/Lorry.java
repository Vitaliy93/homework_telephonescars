package vehicles;

import details.Engine;
import professions.Driver;

public class Lorry extends Car
{
   private int carryingCapacity;

   //
   public Lorry(String carBrand, String carClass, double weight, String engineType, Driver driver, Engine engine, int carryingCapacity)
   {
      super(carBrand, carClass, weight, engineType, driver, engine);
      this.carryingCapacity = carryingCapacity;
   }

   //
   public void setCarryingCapacity(int carryingCapacity)
   {
      this.carryingCapacity = carryingCapacity;
   }

   public double getCarryingCapacity()
   {
      return carryingCapacity;
   }

   //
   @Override
   public void start()
   {
      System.out.println("Починаю тянути прицеп!");
   }

   @Override
   public void stop()
   {
      System.out.println("Зупиняюсь з прицепом!");
   }

   @Override
   public void turnRight()
   {
      System.out.println("Роблю великий радіус повороту з прицепом направо!");
   }

   @Override
   public void turnLeft()
   {
      System.out.println("Роблю великий радіус повороту з прицепом наліво!");
   }

   //
   @Override
   public String toString()
   {
      return "Lorry{" +
              "carryingCapacity=" + carryingCapacity +
              "} " + super.toString();
   }

}
