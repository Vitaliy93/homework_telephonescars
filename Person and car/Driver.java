package professions;

import entities.Person;

public class Driver extends Person
{
    private int drivingExperience;

    public int getDrivingExperience()
    {
        return drivingExperience;
    }

    public void setDrivingExperience(int drivingExperience)
    {
        this.drivingExperience = drivingExperience;
    }


    @Override
    public String toString()
    {
        return "Driver{" +
                "drivingExperience=" + drivingExperience +
                '}';
    }

    public Driver(String fullName, int age, String sex, String numberPhone, int drivingExperience)
    {
        super(fullName, age, sex, numberPhone);
        this.drivingExperience = drivingExperience;
    }

}
