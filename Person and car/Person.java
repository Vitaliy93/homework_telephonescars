package entities;

public class Person
{
    private String fullName;
    private int age;
    private String sex;
    private String numberPhone;


    @Override
    public String toString()
    {
        return "Person{" +
                "fullName='" + fullName + '\'' +
                ", age=" + age +
                ", sex='" + sex + '\'' +
                ", numberPhone='" + numberPhone + '\'' +
                '}';
    }

    public Person(String fullName, int age, String sex, String numberPhone)
   {
         this.fullName = fullName;
         this.sex = sex;
         this.age = age;
         this. numberPhone = numberPhone;
   }



    public String getFullName()

    {
        return fullName;
    }

    public int getAge()
    {
        return age;
    }

    public String getSex()
    {
        return sex;
    }

    public String getNumberPhone()
    {
        return numberPhone;
    }

    public void setFullName(String fullName)
    {
        this.fullName = fullName;
    }

    public void setAge(int age)
    {
        this.age = age;
    }

    public void setSex(String sex)
    {
        this.sex = sex;
    }

}
