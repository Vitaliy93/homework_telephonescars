package vehicles;

import details.Engine;
import professions.Driver;

public class SportCar extends Car
{
    private int maximumSpeed;


    public SportCar(String carBrand, String carClass, double weight, String driverType, String engineType, Driver driver, Engine engine, int maximumSpeed)
    {
        super(carBrand, carClass, weight, driverType, engineType, driver, engine);
        this.maximumSpeed = maximumSpeed;
    }


    public int getMaximumSpeed()
    {
        return maximumSpeed;
    }

    public void setMaximumSpeed(int maximumSpeed)
    {
        this.maximumSpeed = maximumSpeed;
    }
    @Override
    public void start()
    {
        System.out.println("Роблю старт з 0 до 100км. за 5 сек.!");
    }

    @Override
    public void stop()
    {
        System.out.println("Гальмую за допомогою гальмівних колодок Airbus!");
    }

    @Override
    public void turnRight()
    {
        System.out.println("Роблю дріфт направо!");
    }

    @Override
    public void turnLeft()
    {
        System.out.println("Роблю дріфт наліво!");
    }

    @Override
    public String toString()
    {
        return "SportCar{" +
                "maximumSpeed=" + maximumSpeed +
                "} " + super.toString();
    }

}
