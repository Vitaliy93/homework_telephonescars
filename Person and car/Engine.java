package details;

public class Engine
{
       private int power;
       private String manufacture;


    @Override
    public String toString()
    {
        return "Engine{" +
                "power=" + power +
                ", manufacture='" + manufacture + '\'' +
                '}';
    }

    public Engine(int power, String manufacture)
    {
        this.power = power;
        this.manufacture = manufacture;
    }

    public void setPower(int power)
    {
        this.power = power;
    }

    public void setManufacture(String manufacture)
    {
        this.manufacture = manufacture;
    }

    public double getPower() {
        return power;
    }

    public String getManufacture() {
        return manufacture;
    }



}
