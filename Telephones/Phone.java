//Task1
public abstract class Phone
{
    private String name;
    private String model;
    private int storageCapacity;
    private int RAM_Capacity;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getModel()
    {
        return model;
    }

    public void setModel(String model)
    {
        this.model = model;
    }

    public int getStorageCapacity()
    {
        return storageCapacity;
    }

    public void setStorageCapacity(int storageCapacity)
    {
        this.storageCapacity = storageCapacity;
    }

    public int getRAM_Capacity()
    {
        return RAM_Capacity;
    }

    public void setRAM_Capacity(int RAM_Capacity)
    {
        this.RAM_Capacity = RAM_Capacity;
    }

    @Override
    public String toString()
    {
        return "Phone{" +
                "name='" + name + '\'' +
                ", model='" + model + '\'' +
                ", storageCapacity=" + storageCapacity +
                ", RAM_Capacity=" + RAM_Capacity +
                '}';
    }

    public Phone(String name, String model, int storageCapacity, int RAM_Capacity)
    {
        this.name = name;
        this.model = model;
        this.storageCapacity = storageCapacity;
        this.RAM_Capacity = RAM_Capacity;
    }

    public interface PhoneConnection {}

    public interface PhoneMedia {}

}

