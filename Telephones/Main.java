public class Main
{

    public static void main(String[] args)
    {
        //SamsungPhone samsungPhone = new SamsungPhone("Samsung", "S20", 128, 6, true);
        //NokiaPhone nokiaPhone = new NokiaPhone("Nokia", "X30", 256, 8, 5);

        Phone phone1 = new NokiaPhone("Nokia", "X30", 256, 8, 5);
        Phone phone2 = new SamsungPhone("Samsung", "S20", 128, 6, true);

        System.out.println(phone1);
        System.out.println(phone2);

        //String result1 = samsungPhone.toString();
        //System.out.println(result1);
        //
        //String result2 = nokiaPhone.toString();
        //System.out.println(result2);

    }

}